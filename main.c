/*

compile  :  gcc main.c

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct S_fd01
{
    char szId[32];
    float feature_1;
    float feature_2;
    float feature_3;
};

struct S_fd02
{
    char szId[32];
    float feature_4;
    float feature_5;
};

struct S_fd03
{
    char szId[32];
    float feature_1;
    float feature_2;
    float feature_3;
    float feature_4;
    float feature_5;
};

typedef struct S_fd01 _S_fd01;
typedef struct S_fd02 _S_fd02;
typedef struct S_fd03 _S_fd03;

void quicksort(_S_fd02 data[], int first, int last);
int binarySearch(_S_fd02 data[], int l, int r, char *szId);

_S_fd01 data1[1024];
_S_fd02 data2[1024];
_S_fd03 *data3;

int main()
{

    int fd01_total_count = 0;
    int fd02_total_count = 0;

    //read df01
    {
        puts("---------read df01--------");
        FILE *fp;
        fp = fopen("./df01.csv", "r");

        char szBuf[128];
        float a, b, c;
        char *pch;
        int i = 0;
        char szTemp[128];
        fgets(szTemp, 128, fp); //skip first line
        while (fgets(szTemp, 128, fp) != NULL)
        {
            pch = strtok(szTemp, " ,");
            strcpy(szBuf, pch);
            pch = strtok(NULL, " ,");
            a = atof(pch);
            pch = strtok(NULL, " ,");
            b = atof(pch);
            pch = strtok(NULL, " ,");
            c = atof(pch);

            strcpy(data1[i].szId, szBuf);
            data1[i].feature_1 = a;
            data1[i].feature_2 = b;
            data1[i].feature_3 = c;

            printf("%s %f %f %f \n", szBuf, a, b, c);
            i++;
        }

        printf("total : %d\n", i);
        fd01_total_count = i - 1;
        fclose(fp);
    }

    //read df02
    {
        puts("---------read df02--------");
        FILE *fp;
        fp = fopen("./df02.csv", "r");

        char szBuf[128];
        float a, b;
        char *pch;
        int i = 0;
        char szTemp[128];
        fgets(szTemp, 128, fp); //skip first line
        while (fgets(szTemp, 128, fp) != NULL)
        {
            pch = strtok(szTemp, " ,");
            strcpy(szBuf, pch);
            pch = strtok(NULL, " ,");
            a = atof(pch);
            pch = strtok(NULL, " ,");
            b = atof(pch);

            strcpy(data2[i].szId, szBuf);
            data2[i].feature_4 = a;
            data2[i].feature_5 = b;

            printf("%s %f %f  \n", szBuf, a, b);
            i++;
        }

        printf("total : %d\n", i);
        fd02_total_count = i - 1;
        fclose(fp);

        //sort
        // {
        //     puts("--------dump df02");
        //     int i;
        //     for (i = 0; i < fd02_total_count; i++)
        //     {
        //         printf("%s %ld %ld  \n", data2[i].szId, data2[i].feature_4, data2[i].feature_5);
        //     }
        // }

        quicksort(data2, 0, fd02_total_count - 1);

        // {
        //     puts("---------dump df02");
        //     int i;
        //     for (i = 0; i < fd02_total_count; i++)
        //     {
        //         printf("%s %ld %ld  \n", data2[i].szId, data2[i].feature_4, data2[i].feature_5);
        //     }
        // }
    }

    // {
    //     puts("---------binarySearch test");
    //     int i = binarySearch(data2, 0, fd02_total_count - 1, "Shim");
    //     printf("%s %f %f  \n", data2[i].szId, data2[i].feature_4, data2[i].feature_5);
    // }

    //join
    data3 = malloc(sizeof(_S_fd03) * fd01_total_count);

    {
        int _findIndex = 0;
        int i;
        for (i = 0; i < fd01_total_count; i++)
        {
            // // printf("%s %ld \n",data1[i].szId,data1[i].feature_1);
            // printf("%s \n",data1[i].szId);
            _findIndex = binarySearch(data2, 0, fd02_total_count - 1, data1[i].szId);

            printf("%s %f %f  \n", data2[i].szId, data2[_findIndex].feature_4, data2[_findIndex].feature_5);


            strcpy(data3[i].szId, data1[i].szId);
            

            data3[i].feature_1 = data1[i].feature_1;
            data3[i].feature_2 = data1[i].feature_2;
            data3[i].feature_3 = data1[i].feature_3;

            if (_findIndex >= 0)
            {
                //found
                data3[i].feature_4 = data2[_findIndex].feature_4;
                data3[i].feature_5 = data2[_findIndex].feature_5;
            }
            else
            {
                //not found
                data3[i].feature_4 = -999.99;
                data3[i].feature_5 = -999.99;
            }
        }
    }

    {
        puts("--------dump df03");
        int i;
        for (i = 0; i < fd01_total_count; i++)
        {
            
            printf("%s %f %f %f %f %f \n", data3[i].szId,
             data3[i].feature_1, data3[i].feature_2,data3[i].feature_3,
             data3[i].feature_4, data3[i].feature_5);
        }
    }

    {
        puts("--------write df03");

        FILE *fp;
        fp = fopen("./df03.csv", "wt");

        fprintf(fp,"usr_id,feature_1,feature_2,feature_3,feature_4,feature_5\n");
        int i;
        for (i = 0; i < fd01_total_count; i++)
        {
            
            fprintf(fp,"%s,%f,%f,%f,%f,%f\n", data3[i].szId,
             data3[i].feature_1, data3[i].feature_2,data3[i].feature_3,
             data3[i].feature_4, data3[i].feature_5);
        }

        fclose(fp);
    }

    free(data3);

    return 0;
}

//--------------------- liblary

void quicksort(_S_fd02 data[], int first, int last)
{
    _S_fd02 temp;
    int i, j, pivot;
    if (first < last)
    {
        pivot = first;
        i = first;
        j = last;
        while (i < j)
        {
            //while (data[i] <= data[pivot] && i < last)
            while (strcmp(data[i].szId, data[pivot].szId) <= 0 && i < last)
                i++;
            while (strcmp(data[j].szId, data[pivot].szId) > 0)
                j--;
            if (i < j)
            {
                temp = data[i];
                data[i] = data[j];
                data[j] = temp;
            }
        }
        temp = data[pivot];
        data[pivot] = data[j];
        data[j] = temp;
        quicksort(data, first, j - 1);
        quicksort(data, j + 1, last);
    }
}

int binarySearch(_S_fd02 data[], int l, int r, char *szId)
{
    if (r >= l)
    {
        int mid = l + (r - l) / 2;                       // 중간 값 선택
        if (strcmp(data[mid].szId, szId) == 0)           // 비교하여 찾는 값이라면
            return mid;                                  // 해당 인덱스 반환
        if (strcmp(data[mid].szId, szId) > 0)            // 찾으려는 값보다 크다면
            return binarySearch(data, l, mid - 1, szId); // 좌측 배열 탐색
        return binarySearch(data, mid + 1, r, szId);     // 우측 배열 탐색
    }
    return -1;
}
